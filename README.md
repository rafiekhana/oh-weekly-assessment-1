# OH WEEKLY ASSESSMENT 1

In this assessment, you have to create a website view looks like the given design. The purpose of this assessment is to practice your undestanding in basic usage of HTML and CSS to make a website view. You have already provided with some HTML and CSS file that you can use, so you don't have too make it from scratch, and you can just continue it. But, if you want it make it from scratch then do it, whichever make you comfortable to complete this task, you choose. Please deliver this assessment on time no matter what results you make! Thank you.

**Design file**: https://www.figma.com/file/BJ4opqkfVjrFZiPYHnC63E/oh-assessment?node-id=0%3A1

**Deadline**: Friday, 15 October 2021, 23:59 WIB

## Instructions

- Clone this project repository
- Try to complete complete the task
- Make your own git repository (please use `weekly-assessment-1` as the name of project repository)
- Push your result to it
- Fill this form when you are finish: https://forms.gle/NLSnsuddjz1BPzeW6

## Task Requirements

- [ ] Give the document title with `Weekly Assessment 1`
- [ ] Give the document icon with `favicon.ico`
- [ ] Connect `style.css` to `index.html` for applying the style
- [ ] Make website view looks like the design (you don't have to make it pixel perfect)
- [ ] In `header` section put your git username and make it directed in new tab to its profile when clicked
- [ ] In `hero` section make button linked to `location` section
- [ ] In `location` section each card should be directed in new tab to its web, for instance Tokopedia card directed to https://www.tokopedia.com/
- [ ] Push your result in your git repository

Extra miles 🚀🚀🚀

- [ ] Make it responsive in mobile view

## References

- [BEM class naming](http://getbem.com/naming/)
- [Media queries](https://css-tricks.com/a-complete-guide-to-css-media-queries/)
- [Responsive web designs](https://www.freecodecamp.org/news/taking-the-right-approach-to-responsive-web-design/)
